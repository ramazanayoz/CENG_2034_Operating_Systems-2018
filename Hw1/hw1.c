#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
    printf("heyy kernel im here: %d\n", (int) getpid());

pid_t pid = fork();



if (pid<0) { 
 	printf("forked failed");
}

else if (pid==0){

    printf("right child process created id: %d\n", (int) pid);
	sleep(1);
	printf("right child existing\n");
	exit(0);	
}
	
else{ //parent 
	wait(NULL);

  pid_t pid2 = fork(); 

	if (pid2<0) { 
 		printf("forked failed");
	}

	else if (pid2==0){

    	printf("left child process created of id: %d\n", (int) pid2);
	sleep(1);
	printf("left child existing\n");
	exit(0);	
}
else{ //parent
	wait(NULL);
	printf("parent ending\n");
}}
return 0;

}
